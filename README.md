<!--required for Modrinth centering -->
<center>
<div style="text-align:center;line-height:250%">

## Leaves Us In Peace

[![Minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_565173_all.svg)](https://modrinth.com/mod/leaves-us-in-peace/versions#all-versions)
![environment: server](https://img.shields.io/badge/environment-server-orangered)  
[![loader: Fabric](https://img.shields.io/badge/loader-Fabric-cdc4ae?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjYgMjgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48cGF0aCBkPSJtMTAgMjQtOC04TDE0IDRWMmgybDYgNnY0TDEwIDI0eiIgc3R5bGU9ImZpbGw6I2RiZDBiNCIvPjxwYXRoIGQ9Ik0xMiA2djJoLTJ2Mkg4djJINnYySDR2MkgydjRoMnYyaDJ2MmgydjJoMnYtMmgydi0yaDJ2LTJoMnYtMmgydi0yaDJ2LTRoLTJ2LTJoLTJWOGgtMlY2aDJ2MmgydjJoMnYyaDJ2Mmgydi0yaC0yVjhoLTJWNmgtMlY0aC0yVjJoMnYyaDJ2MmgydjJoMnYyaDJ2NGgtMnYyaC00djJoLTJ2MmgtMnYyaC0ydjRoLTJ2Mkg4di0ySDZ2LTJINHYtMkgydi0ySDB2LTRoMnYtMmgydi0yaDJ2LTJoMlY4aDJWNmgyVjJoMlYwaDJ2MmgtMnY0aC0yIiBzdHlsZT0iZmlsbDojMzgzNDJhIi8+PHBhdGggc3R5bGU9ImZpbGw6IzgwN2E2ZCIgZD0iTTIyIDEyaDJ2MmgtMnoiLz48cGF0aCBkPSJNMiAxOGgydjJoMnYyaDJ2MmgydjJIOHYtMkg2di0ySDR2LTJIMnYtMiIgc3R5bGU9ImZpbGw6IzlhOTI3ZSIvPjxwYXRoIGQ9Ik0yIDE2aDJ2MmgydjJoMnYyaDJ2Mkg4di0ySDZ2LTJINHYtMkgydi0yeiIgc3R5bGU9ImZpbGw6I2FlYTY5NCIvPjxwYXRoIGQ9Ik0yMiAxMnYtMmgtMlY4aC0yVjZsLTIuMDIzLjAyM0wxNiA4aDJ2MmgydjJoMnpNMTAgMjR2LTJoMnYtNGg0di00aDJ2LTJoMnY0aC0ydjJoLTJ2MmgtMnYyaC0ydjJoLTIiIHN0eWxlPSJmaWxsOiNiY2IyOWMiLz48cGF0aCBkPSJNMTQgMThoLTR2LTJIOHYtMmgydjJoNHYyem00LTRoLTR2LTJoLTJ2LTJoLTJWOGgydjJoMnYyaDR2MnpNMTQgNGgydjJoLTJWNHoiIHN0eWxlPSJmaWxsOiNjNmJjYTUiLz48L3N2Zz4=)](https://fabricmc.net/)
<a href="https://quiltmc.org/"><img alt="available for: Quilt Loader" width=73 src="https://raw.githubusercontent.com/QuiltMC/art/master/brand/1024png/quilt_available_dark.png"></a>  
<a href="https://modrinth.com/mod/fabric-api/versions"><img alt="Requires: Fabric API" width="60" src="https://i.imgur.com/Ol1Tcf8.png"></a>
[![supports: Cloth Config](https://img.shields.io/badge/supports-Cloth_Config-89dd43?logo=data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHZpZXdCb3g9IjAgMCAyNjQgMjY0IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiPjxwYXRoIGlkPSJiYWNrZ3JvdW5kIiBkPSJNODQsMjA3bC0zNiwtMzZsMCwtMzlsMTgsLTE4bDE4LC0yMWwyMSwtMThsMzYsLTM2bDAsLTIxbDIxLDBsMTgsMjFsMzYsMzZsMCwxOGwtNTQsNzhsLTM5LDM2bC0zOSwwWiIgc3R5bGU9ImZpbGw6IzljZmY1NTsiLz48cGF0aCBpZD0ib3V0bGluZSIgZD0iTTE2MiwxOGwwLC0xOGwtMjEsMGwwLDE4bC0xOCwwbDAsMzlsLTE4LDBsMCwxOGwtMjEsMGwwLDE4bC0xOCwwbDAsMjFsLTE4LDBsMCwxOGwtMTgsMGwwLDE4bC0yMSwwbDAsMzlsMjEsMGwwLDE4bDE4LDBsMCwxOGwxOCwwbDAsMjFsMTgsMGwwLDE4bDIxLDBsMCwtMThsMTgsMGwwLC0yMWwxOCwwbDAsLTE4bDIxLDBsMCwtMThsMTgsMGwwLC0xOGwxOCwwbDAsLTIxbDM5LDBsMCwtMThsMTgsMGwwLC0zOWwtMTgsMGwwLC0xOGwtMjEsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMTgsMGwwLC0yMWwtMTgsMGwwLDIxbDE4LDBsMCwxOGwxOCwwbDAsMThsMTgsMGwwLDM5bDIxLDBsMCwxOGwtMjEsMGwwLC0xOGwtMTgsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMjEsMGwwLDE4bDIxLDBsMCwxOGwxOCwwbDAsMjFsMTgsMGwwLDM2bC0xOCwwbDAsMjFsLTE4LDBsMCwxOGwtMjEsMGwwLDE4bC0xOCwwbDAsMThsLTE4LDBsMCwyMWwtMjEsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMTgsMGwwLC0zOWwxOCwwbDAsLTE4bDE4LDBsMCwtMThsMTgsMGwwLC0yMWwyMSwwbDAsLTE4bDE4LDBsMCwtMThsMTgsMGwwLC0zOWwyMSwwIiBzdHlsZT0iZmlsbDojMTE1MTEwOyIvPjxwYXRoIGlkPSJzaGFkZS0xIiBzZXJpZjppZD0ic2hhZGUgMSIgZD0iTTQ4LDE3MWwtMTgsMGwwLDE4bDE4LDBsMCwxOGwxOCwwbDAsMThsMTgsMGwwLDIxbDIxLDBsMCwtMjFsLTIxLDBsMCwtMThsLTE4LDBsMCwtMThsLTE4LDBsMCwtMThabTE2OCwtNTdsMjEsMGwwLDE4bC0yMSwwbDAsLTE4WiIgc3R5bGU9ImZpbGw6IzZlYWEzMzsiLz48cGF0aCBpZD0ic2hhZGUtMiIgc2VyaWY6aWQ9InNoYWRlIDIiIGQ9Ik00OCwxNTBsLTE4LDBsMCwyMWwxOCwwbDAsMThsMTgsMGwwLDE4bDE4LDBsMCwxOGwyMSwwbDAsLTE4bC0yMSwwbDAsLTE4bC0xOCwwbDAsLTE4bC0xOCwwbDAsLTIxWiIgc3R5bGU9ImZpbGw6IzgwY2MzZDsiLz48cGF0aCBpZD0ic2hhZGUtMyIgc2VyaWY6aWQ9InNoYWRlIDMiIGQ9Ik0xMDUsMjI1bDAsLTE4bDE4LDBsMCwtMzZsMzksMGwwLC0zOWwxOCwwbDAsLTE4bDE4LDBsMCwzNmwtMTgsMGwwLDIxbC0xOCwwbDAsMThsLTIxLDBsMCwxOGwtMTgsMGwwLDE4bC0xOCwwWm0xMTEsLTExMWwtMTgsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwxOCwwbDAsMThsMTgsMGwwLDE4bDE4LDBsMCwyMVoiIHN0eWxlPSJmaWxsOiM4OWRjNDI7Ii8+PHBhdGggaWQ9InNoYWRlLTQiIHNlcmlmOmlkPSJzaGFkZSA0IiBkPSJNMTQxLDE3MWwtMzYsMGwwLC0yMWwtMjEsMGwwLC0xOGwyMSwwbDAsMThsMzYsMGwwLDIxWm0zOSwtMzlsLTM5LDBsMCwtMThsLTE4LDBsMCwtMjFsLTE4LDBsMCwtMThsMTgsMGwwLDE4bDE4LDBsMCwyMWwzOSwwbDAsMThabS0xOCwtNzVsLTIxLDBsMCwtMThsMjEsMGwwLDE4WiIgc3R5bGU9ImZpbGw6IzkwZWI0YjsiLz48L3N2Zz4=)](https://modrinth.com/mod/cloth-config/versions)
[![supports: Mod Menu](https://img.shields.io/badge/supports-Mod_Menu-134bfe?logo=data:image/webp+xml;base64,UklGRlIAAABXRUJQVlA4TEUAAAAv/8F/AA9wpv9T/M///McDFLeNpKT/pg8WDv6jiej/BAz7v+bbAKDn9D9l4Es/T/9TBr708/Q/ZeBLP0//c7Xiqp7z/QEA)](https://modrinth.com/mod/modmenu/versions)
[![supports: SSS Translate](https://img.shields.io/badge/supports-SSS_Translate-253137?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMzAwIDMwMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48dXNlIHhsaW5rOmhyZWY9IiNhIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIvPjxkZWZzPjxpbWFnZSBpZD0iYSIgd2lkdGg9IjMwMCIgaGVpZ2h0PSIzMDAiIHhsaW5rOmhyZWY9ImRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBU3dBQUFFc0NBWUFBQUI1Zlk1MUFBQUFDWEJJV1hNQUFBN0VBQUFPeEFHVkt3NGJBQUFGbUVsRVFWUjRuTzNkSWRLazFSbUc0ZmVrV3NWZ0NDSUdOU09pRUdoMC9wanNaRmhBWEZqQllLS3pnSWdZR0QwN2lLT1lpTUVnUUNGR0lVNEVMS0NwNnE2VHUrZTZGdkRXVTlWVjk4d3gvN2YyM251b2VMN1dlblBMZzN2dkZ6UHo4cFkzdVp0WGE2Mm4weU5PK3QzcEFRRFhFaXdnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJRU93Z0F6QkFqSXVkN3I3cjVsNWZhZmI3N01mVGcvNERUNC9QZUFCdlQwOTRMUjdCZXYxV3V2TE85MG13Ty9QUFhnU0FobUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRY2ErLzZmNkh2ZmV6TzkxK243MWRhLzE4ZXNRMS9QNTM4VzZ0OWYzcEVTZXR2ZmMrUFlLclBWOXJ2Ym5sd2IzM2k1bDVlY3ViM00ycnRkYlQ2UkVuZVJJQ0dZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VER1pXYitjbnJFQS9wMFp2NStlc1NWL2owek4vMndCVFB6eSsvLzZla1JqK2F5MXZyNjlJaEhVL29RMFZycnU1bjU3dlNPUi9QcjE0aTRNVTlDSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlMbnZ2cDlNakhsRG00d043NzQ5bjVrK25kenlnRDA4UGVFU1htZm5xOUFpTyt1dk12RHc5QXE3aFNRaGtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbHI3LzNzOUFpdTluYXQ5Zk10RCs2OVA1aVpqMjU1azd0NXQ5YjYvdlFJQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBRCtQNjI5OTR2VEk3amFQOWRhUDkzeTRONzdrNW41N0pZMzRWN1czbnVmSHNIVm5xKzEzdHp5NEsvL1lMMjg1VTI0Rjk4bEJESUVDOGdRTENCRHNJQU13UUl5QkF2SUVDd2dRN0NBRE1FQ01nUUx5QkFzSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQWpNdWQ3bjYrMXZyeVRyZUI5NVQvWVFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUU1iYWUrODczSDA5TS8rNXc5MzMzUmRyclI5dmVYRHYvY25NZkhiTG16RWZ6Y3pmVG8rNDBqY3o4NC9USTA2NlY3QzRqK2RyclRlblJ6eVN2ZmV6bWZuMjlJNHJ2VnByUFowZWNaSW5JWkFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdaZVorZS9wRVEvbzl6UHp4OU1qNE5GYzFsclBUbzk0Tkh2dnA1bjU2dlFPZURTZWhFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tISFplMzk5ZXNRRCt2RDBBSGhFbDVuNTgra1JBTmZ3SkFReUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJT015TTY5T2orQnE3MDRQZ0pNdWE2Mm4weU1BcnVGSkNHUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRURHL3dCN1lYaUZyYXU1NUFBQUFBQkpSVTVFcmtKZ2dnPT0iLz48L2RlZnM+PC9zdmc+)](https://modrinth.com/mod/sss-translate)  
[![license: CC0](https://img.shields.io/badge/license-CC0-white)](https://gitlab.com/supersaiyansubtlety/leaves_us_in_peace/-/blob/master/LICENSE)
[![source on: GitLab](https://img.shields.io/badge/source_on-GitLab-fc6e26?logo=gitlab)](https://gitlab.com/supersaiyansubtlety/leaves_us_in_peace)
[![issues: GitLab](https://img.shields.io/gitlab/issues/open-raw/supersaiyansubtlety/leaves_us_in_peace?label=issues&logo=gitlab)](https://gitlab.com/supersaiyansubtlety/leaves_us_in_peace/-/issues)
[![localized: Percentage](https://badges.crowdin.net/leaves-us-in-peace/localized.svg)](https://crwd.in/leaves-us-in-peace)  
[![Modrinth: Downloads](https://img.shields.io/modrinth/dt/leaves-us-in-peace?logo=modrinth&label=Modrinth&color=00ae5d)](https://modrinth.com/mod/leaves-us-in-peace/versions)
[![CurseForge: Downloads](https://img.shields.io/curseforge/dt/565173?logo=curseforge&label=CurseForge&color=f16437)](https://www.curseforge.com/minecraft/mc-mods/leaves-us-in-peace/files)  
[![chat: Discord](https://img.shields.io/discord/1006391289006280746?logo=discord&color=5964f3)](https://discord.gg/xABmPngXAH)
<a href="https://coindrop.to/supersaiyansubtlety"><img alt="coindrop.to me" width="82" style="border-radius:3px" src="https://coindrop.to/embed-button.png"></a>
<a href="https://ko-fi.com/supersaiyansubtlety"><img alt="Buy me a coffee" width="106" src="https://i.ibb.co/4gwRR8L/p.png"></a>

</div>
</center>

---

### Leaf decay ignores different types of leaves and logs. Fast leaf decay. Configurable. Data driven.

Works server-side and in single player.

This mod aims to make leaf decay less frustrating.

It started out as a fork of [Tfarcenim's](https://github.com/Tfarcenim) [LeafMeAlone](https://github.com/Tfarcenim/LeafMeAlone), but then I overhauled it to the point of it being an almost entirely different mod.

It does 3 (+1) things to change how leaves decay, all configurable:
1. Leaves will ignore leaves of a different type when determining whether to decay or not. By default, this will only
match identical leaves (like in LeafMeAlone), but if a tag for leaves is found, all leaves in that tag will match.
This is how the mod handles azaleas' two types of leaves. Mods (or mod users) can add compat for trees with multiple
types of leaves by just adding a tag. Details on
[the wiki](https://gitlab.com/supersaiyansubtlety/leaves_us_in_peace/-/wikis/home).
2. Leaves will ignore logs from different tree types when determining whether to decay or not. This will look for a tag
by the name of the log and check if it contains the leaves doing the check. If no tag is found, any log will match
instead (like in vanilla). Details on
[the wiki](https://gitlab.com/supersaiyansubtlety/leaves_us_in_peace/-/wikis/home).
3. Leaves will decay much faster. This is basically like the fast leaf decay mods you've likely seen before. It's
highly configurable, and uses an algorithm that can check for diagonal leaves, in addition to the usual check for
adjacent leaves, so it's less likely to leave a stray leaf or two floating.
- 3 + 1. This isn't convenient or less frustrating, but leaves can optionally make block breaking sounds and create
particles when they decay.

---

<details>

<summary>Configuration</summary>

Supports [Cloth Config](https://modrinth.com/mod/cloth-config/versions) and
[Mod Menu](https://modrinth.com/mod/modmenu/versions) for configuration, but neither is required.

Options will use their default values if [Cloth Config](https://modrinth.com/mod/cloth-config/versions) is absent.

If Cloth Config is present, options can be configured either through
[Mod Menu](https://modrinth.com/mod/modmenu/versions) or by editing `config/leaves_us_in_peace.json` in your
instance folder (`.minecraft/` by default for the vanilla launcher).

- Match leaves types; default: `true`
  > Leaves ignore leaves of other types when determining whether to decay or not

- Unknown leaves only match self; default: `true`
  > If "Yes", leaves without tags defining which leaves they should match will only match leaves of exactly the same type.
  If "No", leaves without tags will match all leaves.
  Set to "No" if you're having trouble with some modded leaves decaying as soon their tree grows (with multiple leaves types).

- Match logs to leaves; default: `true`
  > Leaves ignore logs of other tree types when determining whether to decay or not

- Ignore persistent leaves; default: `true`
  > Leaves ignore persistent leaves (placed by player) when determining whether to decay or not

- Accelerate leaves decay; default: `true`
  > Make leaves decay much faster

- Decay delay
  > Random delay between leaves decaying and updating nearby leaves.
  Has no effect if 'Accelerate leaves decay' is 'No'.

    - Minimum; default: `10`
      > Minimum random delay. Set minimum equal to maximum to eliminate randomness.

    - Maximum; default: `60`
      > Maximum random delay

- Update diagonal leaves; default: `true`
  > When leaves decay, update leaves diagonal to them in addition to those adjacent to them

- Do decaying leaves effects; default: `false`
  > Create particles and sounds when leaves decay

</details>

---

<details>

<summary>Compatibility</summary>

Tags for trees added by several mods are included:

- [Oh The Biomes We've Gone](https://modrinth.com/mod/oh-the-biomes-weve-gone)
- [Oh The Biomes You'll Go](https://modrinth.com/mod/biomesyougo)
- [Promenade](https://modrinth.com/mod/promenade)
- [Traverse](https://modrinth.com/mod/traverse)
- [William Wythers' Overhauled Overworld](https://modrinth.com/mod/wwoo)

The built-in data pack `wood_prevents_decay` (default disabled) can also make vanilla wood blocks (logs with
bark on all 6 sides) count as logs for their respective trees, since some mods generate trees with wood blocks.

Further modded tree support can be added via data packs, find out how on
[the wiki](https://gitlab.com/supersaiyansubtlety/leaves_us_in_peace/-/wikis/home).

Leaves Us In Peace does its best to handle unknown trees (modded trees without tags) gracefully:
- Unknown leaves will only consider leaves of exactly the same type to be part of their tree
(this can be changed by disabling "Unknown leaves only match self")
- Unknown logs will prevent any adjacent unknown leaves from decaying

However it's not perfect: modded trees that aren't implemented similarly to vanilla trees or that don't follow vanilla
tag conventions can sometimes decay unexpectedly. If you encounter this, please report it on the
[issue tracker](https://gitlab.com/supersaiyansubtlety/leaves_us_in_peace/-/issues)
(please *don't* report it to the mod that adds the unexpectedly decaying tress).

</details>

---

<details>

<summary>Translations</summary>

[![localized: Percentage](https://badges.crowdin.net/leaves-us-in-peace/localized.svg)](https://crwd.in/leaves-us-in-peace) [![supports: SSS Translate](https://img.shields.io/badge/supports-SSS_Translate-253137?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMzAwIDMwMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48dXNlIHhsaW5rOmhyZWY9IiNhIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIvPjxkZWZzPjxpbWFnZSBpZD0iYSIgd2lkdGg9IjMwMCIgaGVpZ2h0PSIzMDAiIHhsaW5rOmhyZWY9ImRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBU3dBQUFFc0NBWUFBQUI1Zlk1MUFBQUFDWEJJV1hNQUFBN0VBQUFPeEFHVkt3NGJBQUFGbUVsRVFWUjRuTzNkSWRLazFSbUc0ZmVrV3NWZ0NDSUdOU09pRUdoMC9wanNaRmhBWEZqQllLS3pnSWdZR0QwN2lLT1lpTUVnUUNGR0lVNEVMS0NwNnE2VHUrZTZGdkRXVTlWVjk4d3gvN2YyM251b2VMN1dlblBMZzN2dkZ6UHo4cFkzdVp0WGE2Mm4weU5PK3QzcEFRRFhFaXdnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJRU93Z0F6QkFqSXVkN3I3cjVsNWZhZmI3N01mVGcvNERUNC9QZUFCdlQwOTRMUjdCZXYxV3V2TE85MG13Ty9QUFhnU0FobUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRY2ErLzZmNkh2ZmV6TzkxK243MWRhLzE4ZXNRMS9QNTM4VzZ0OWYzcEVTZXR2ZmMrUFlLclBWOXJ2Ym5sd2IzM2k1bDVlY3ViM00ycnRkYlQ2UkVuZVJJQ0dZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VER1pXYitjbnJFQS9wMFp2NStlc1NWL2owek4vMndCVFB6eSsvLzZla1JqK2F5MXZyNjlJaEhVL29RMFZycnU1bjU3dlNPUi9QcjE0aTRNVTlDSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlMbnZ2cDlNakhsRG00d043NzQ5bjVrK25kenlnRDA4UGVFU1htZm5xOUFpTyt1dk12RHc5QXE3aFNRaGtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbHI3LzNzOUFpdTluYXQ5Zk10RCs2OVA1aVpqMjU1azd0NXQ5YjYvdlFJQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBRCtQNjI5OTR2VEk3amFQOWRhUDkzeTRONzdrNW41N0pZMzRWN1czbnVmSHNIVm5xKzEzdHp5NEsvL1lMMjg1VTI0Rjk4bEJESUVDOGdRTENCRHNJQU13UUl5QkF2SUVDd2dRN0NBRE1FQ01nUUx5QkFzSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQWpNdWQ3bjYrMXZyeVRyZUI5NVQvWVFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUU1iYWUrODczSDA5TS8rNXc5MzMzUmRyclI5dmVYRHYvY25NZkhiTG16RWZ6Y3pmVG8rNDBqY3o4NC9USTA2NlY3QzRqK2RyclRlblJ6eVN2ZmV6bWZuMjlJNHJ2VnByUFowZWNaSW5JWkFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdaZVorZS9wRVEvbzl6UHp4OU1qNE5GYzFsclBUbzk0Tkh2dnA1bjU2dlFPZURTZWhFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tISFplMzk5ZXNRRCt2RDBBSGhFbDVuNTgra1JBTmZ3SkFReUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJT015TTY5T2orQnE3MDRQZ0pNdWE2Mm4weU1BcnVGSkNHUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRURHL3dCN1lYaUZyYXU1NUFBQUFBQkpSVTVFcmtKZ2dnPT0iLz48L2RlZnM+PC9zdmc+)](https://modrinth.com/mod/sss-translate)  
Install [SSS Translate](https://modrinth.com/mod/sss-translate) to automatically download new translations.  
You can help translate Leaves Us In Peace on [Crowdin](https://crwd.in/leaves-us-in-peace).

</details>

---

<details>

<summary>Credits</summary>

- [Tfarcenim's](https://github.com/Tfarcenim) for making [LeafMeAlone](https://github.com/Tfarcenim/LeafMeAlone).

- [gliscowo](https://github.com/gliscowo) for making [Isometric Renders](https://modrinth.com/mod/isometric-renders),
which I used to create the mod icon.

</details>

---

This mod is only for Fabric (works on Quilt, too!) and I won't be porting it to Forge. The license is CC0, however,
so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
