package net.sssubtlety.leaves_us_in_peace.mixin;

import net.sssubtlety.leaves_us_in_peace.LeavesUsInPeace;
import net.sssubtlety.leaves_us_in_peace.mixin_helper.PackedTicksMixinAccessor;
import net.sssubtlety.leaves_us_in_peace.mixin_helper.WorldChunkMixinAccessor;

import net.minecraft.block.LeavesBlock;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtList;
import net.minecraft.registry.DynamicRegistryManager;
import net.minecraft.registry.Registries;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.HeightLimitView;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.ChunkSerializer;
import net.minecraft.world.chunk.WorldChunk;
import net.minecraft.world.tick.ChunkTickScheduler;
import net.minecraft.world.tick.Tick;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.sugar.Local;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Mixin(ChunkSerializer.class)
abstract class ChunkSerializerMixin {
    @Shadow @Final private Chunk.PackedTicks packedTicks;
    @Unique
    private static final String LEAVES_DECAY_SCHEDULER_KEY = LeavesUsInPeace.NAMESPACE + ":leaves_decay_ticks";

    @Unique
    private static Optional<LeavesBlock> parseLeavesBlock(String string) {
        return Registries.BLOCK.getOrEmpty(Identifier.tryParse(string)).flatMap(block ->
            block instanceof LeavesBlock leavesBlock ?
                Optional.of(leavesBlock) :
                Optional.empty()
        );
    }

    @ModifyExpressionValue(
        method = "fromNbt",
        at = @At(
            value = "NEW",
            target = "(Ljava/util/List;Ljava/util/List;)Lnet/minecraft/world/chunk/Chunk$PackedTicks;"
        )
    )
    private static Chunk.PackedTicks deserializeAndPackLeavesDecayTicks(
        Chunk.PackedTicks originalPackedTicks,
        HeightLimitView world, DynamicRegistryManager registryManager, NbtCompound nbt,
        @Local ChunkPos chunkPos
    ) {
        // not present for non-WorldChunk or pre-mod Chunks
        if (nbt.contains(LEAVES_DECAY_SCHEDULER_KEY)) {
            ((PackedTicksMixinAccessor) (Object) originalPackedTicks)
                .leaves_us_in_peace$setLeavesDecayTicks(Tick.listFromNbt(
                    nbt.getList(LEAVES_DECAY_SCHEDULER_KEY, NbtElement.COMPOUND_TYPE),
                    ChunkSerializerMixin::parseLeavesBlock,
                    chunkPos
                ));
        }

        return originalPackedTicks;
    }

    @ModifyExpressionValue(
        method = "createProtoChunk",
        at = @At(
            value = "NEW",
            target = "(Lnet/minecraft/world/World;Lnet/minecraft/util/math/ChunkPos;" +
                "Lnet/minecraft/world/chunk/UpgradeData;Lnet/minecraft/world/tick/ChunkTickScheduler;" +
                "Lnet/minecraft/world/tick/ChunkTickScheduler;J[Lnet/minecraft/world/chunk/ChunkSection;" +
                "Lnet/minecraft/world/chunk/WorldChunk$PostLoadProcessor;Lnet/minecraft/world/gen/chunk/BlendingData;" +
                ")Lnet/minecraft/world/chunk/WorldChunk;"
        )
    )
    private WorldChunk unpackTicksAndSetLeavesDecayScheduler(WorldChunk original) {
        ((WorldChunkMixinAccessor) original).leaves_us_in_peace$setLeavesDecayTickScheduler(
            new ChunkTickScheduler<>(
                ((PackedTicksMixinAccessor) (Object) this.packedTicks).leaves_us_in_peace$getLeavesDecayTicks()
            )
        );

        return original;
    }

    @Inject(method = "putTicks", at = @At("TAIL"))
    private static void unpackAndSerializeLeavesDecayTicks(
        NbtCompound nbtCompound, Chunk.PackedTicks packedTicks, CallbackInfo ci
    ) {
        final List<Tick<LeavesBlock>> leavesDecayTicks =
            ((PackedTicksMixinAccessor) (Object) packedTicks).leaves_us_in_peace$getLeavesDecayTicks();

        // empty for non-WorldChunk Chunks
        if (!leavesDecayTicks.isEmpty()) {
            nbtCompound.put(
                LEAVES_DECAY_SCHEDULER_KEY,
                leavesDecayTicks.stream()
                    .map(tick -> tick.toNbt(block -> Registries.BLOCK.getId(block).toString()))
                    .collect(Collectors.toCollection(NbtList::new))
            );
        }
    }
}
