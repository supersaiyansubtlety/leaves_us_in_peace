package net.sssubtlety.leaves_us_in_peace.mixin;

import com.llamalad7.mixinextras.injector.wrapmethod.WrapMethod;
import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import net.minecraft.registry.Registries;
import net.minecraft.registry.tag.BlockTags;
import net.minecraft.registry.tag.TagKey;
import net.sssubtlety.leaves_us_in_peace.LeavesUsInPeace;
import net.sssubtlety.leaves_us_in_peace.mixin_helper.LeavesBlockMixinAccessor;
import net.sssubtlety.leaves_us_in_peace.mixin_helper.ServerWorldMixinAccessor;

import org.jetbrains.annotations.NotNull;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.LeavesBlock;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.Property;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.random.RandomGenerator;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.WorldView;
import net.minecraft.world.tick.TickSchedulerAccess;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Optional;

import static net.sssubtlety.leaves_us_in_peace.FeatureControl.getDecayDelay;
import static net.sssubtlety.leaves_us_in_peace.FeatureControl.leavesMatch;
import static net.sssubtlety.leaves_us_in_peace.FeatureControl.shouldAccelerateLeavesDecay;
import static net.sssubtlety.leaves_us_in_peace.FeatureControl.shouldDoDecayingLeavesEffects;
import static net.sssubtlety.leaves_us_in_peace.FeatureControl.shouldIgnorePersistentLeaves;
import static net.sssubtlety.leaves_us_in_peace.FeatureControl.shouldMatchLeavesTypes;
import static net.sssubtlety.leaves_us_in_peace.FeatureControl.shouldMatchLogsToLeaves;
import static net.sssubtlety.leaves_us_in_peace.FeatureControl.shouldUpdateDiagonalLeaves;
import static net.sssubtlety.leaves_us_in_peace.LeavesUsInPeace.LOGS_WITHOUT_LEAVES;
import static net.sssubtlety.leaves_us_in_peace.mixin.accessor.DirectionAccessor.leaves_us_in_peace$getHORIZONTAL;

@Mixin(LeavesBlock.class)
abstract class LeavesBlockMixin extends Block implements LeavesBlockMixinAccessor {
	@Unique
	@NotNull
	private static final ThreadLocal<Optional<BlockState>> currentLeaves =
		ThreadLocal.withInitial(Optional::empty);

	@Shadow
	@Final
	public static BooleanProperty PERSISTENT;

	@Shadow
	protected abstract boolean canDecay(BlockState state);

	private LeavesBlockMixin() {
        //noinspection DataFlowIssue
        super(null);
		throw new IllegalStateException("Dummy constructor called!");
	}

	@Override
	public void leaves_us_in_peace$tryDecaying(
		ServerWorld world, BlockPos pos, BlockState state, RandomGenerator random
	) {
		if (shouldAccelerateLeavesDecay(state) && this.canDecay(state)) {
			dropStacks(state, world, pos);
			world.removeBlock(pos, false);
			this.trySpawningDecayEffects(state, world, pos);

			if (shouldUpdateDiagonalLeaves()) {
				getDiagonalPositions(pos).forEach(diagonalPos -> {
					final BlockState diagonalState = world.getBlockState(diagonalPos);
					if (
						diagonalState != null &&
						diagonalState.getBlock() instanceof LeavesBlock diagonalLeaves &&
						leavesMatch(diagonalState, this.getDefaultState())
					) {
						world.scheduleBlockTick(diagonalPos, diagonalLeaves, 0);
					}
				});
			}
		}
	}

	@Inject(method = "getStateForNeighborUpdate",at = @At(value = "HEAD"))
	private void captureNeighborBlock(
		BlockState state, WorldView world, TickSchedulerAccess tickSchedulerAccess, BlockPos pos, Direction direction,
		BlockPos neighborPos, BlockState neighborState, RandomGenerator random,
		CallbackInfoReturnable<BlockState> cir
	) {
		if (shouldMatchLeavesTypes()) {
            LeavesUsInPeace.updateLeavesGroups(this);
        }

		currentLeaves.set(Optional.of(state));
	}

	@Inject(method = "getStateForNeighborUpdate",at = @At(value = "TAIL"))
	private void resetCapturedNeighborBlock(CallbackInfoReturnable<BlockState> cir) {
		currentLeaves.remove();
	}

	@WrapMethod(method = "updateDistanceFromLogs")
	private static BlockState captureUpdatingBlock(
		BlockState state, WorldAccess world, BlockPos pos,
		Operation<BlockState> original
	) {
		if (shouldMatchLeavesTypes()) {
			LeavesUsInPeace.updateLeavesGroups(state.getBlock());
		}

		currentLeaves.set(Optional.of(state));

		final BlockState newState = original.call(state, world, pos);

		currentLeaves.remove();

		return newState;
	}

	@ModifyArg(
		method = "updateDistanceFromLogs",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/block/LeavesBlock;getDistanceFromLog(Lnet/minecraft/block/BlockState;)I"
		)
	)
	private static BlockState checkBlockState(BlockState state) {
		if (shouldMatchLogsToLeaves()) {
			LeavesUsInPeace.updateTreeTypes(state);
		}

		return state;
	}

	// If a tree tag is found, match it. Otherwise, match all logs like vanilla
	@Redirect(
		method = "getOptionalDistanceFromLog",
		at = @At(
			value = "INVOKE", ordinal = 0,
			target = "Lnet/minecraft/block/BlockState;isIn(Lnet/minecraft/registry/tag/TagKey;)Z"
		),
		slice = @Slice(
			from = @At(
				value = "FIELD",
				target = "Lnet/minecraft/registry/tag/BlockTags;LOGS:Lnet/minecraft/registry/tag/TagKey;"
			)
		)
	)
	private static boolean tryMatchLog(BlockState state, TagKey<Block> tagKey) {
		if (shouldMatchLogsToLeaves()) {
			if (state.isIn(LOGS_WITHOUT_LEAVES)) {
				return false;
			}

			final Optional<BlockState> leaves = currentLeaves.get();
			if (leaves.isPresent()) {
				final Block block = state.getBlock();
				final TagKey<Block> treeType = LeavesUsInPeace.getTreeType(block);
				if (
					treeType != null &&
					Registries.BLOCK.getTag(treeType).isPresent()
				) {
                    return leaves.get().isIn(treeType);
				}
			}
		}

		return state.isIn(BlockTags.LOGS);
	}

	@WrapOperation(
		method = "getOptionalDistanceFromLog",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/block/BlockState;contains(Lnet/minecraft/state/property/Property;)Z"
		)
	)
	private static boolean matchLeaves(
		BlockState otherLeavesState, Property<?> property, Operation<Boolean> original
	) {
		if (
			!original.call(otherLeavesState, property) ||
			currentLeaves.get().isEmpty()
		) {
			return false;
		}

		final BlockState currentLeavesState = currentLeaves.get().get();

		if (shouldIgnorePersistentLeaves()) {
			// non-persistent leaves only care about other non-persistent leaves,
			//   persistent leaves care about BOTH non/persistent leaves
			if (
				!currentLeavesState.getOrThrow(PERSISTENT) &&
				otherLeavesState.getOrEmpty(PERSISTENT).orElse(false)
			) {
				return false;
			}
		}

		if (shouldMatchLeavesTypes()) {
            return leavesMatch(otherLeavesState, currentLeavesState);
		}

		return true;
	}

    @WrapOperation(
		method = "scheduledTick",
		at = @At(
			value = "INVOKE",
			target = "Lnet/minecraft/server/world/ServerWorld;setBlockState(Lnet/minecraft/util/math/BlockPos;" +
				"Lnet/minecraft/block/BlockState;I)Z"
		)
	)
	private boolean tryAcceleratingDecay(
		ServerWorld world, BlockPos pos, BlockState newState, int flags, Operation<Boolean> original,
		BlockState oldState, ServerWorld duplicate1, BlockPos duplicate2, RandomGenerator random
	) {
		final boolean originalReturn = original.call(world, pos, newState, flags);

		// checking that the old state cannot decay is important for trees that generate with
		// leaves with incorrect distances
		if (this.canDecay(newState) && !this.canDecay(oldState)) {
			((ServerWorldMixinAccessor) world).leaves_us_in_peace$scheduleLeavesDecayTick(
				pos, (LeavesBlock)(Object) this, getDecayDelay(random)
			);
		}

		return originalReturn;
	}

	@Inject(
		method = "randomTick",
		at = @At(
			value = "INVOKE", shift = At.Shift.AFTER,
			target = "Lnet/minecraft/server/world/ServerWorld;removeBlock(Lnet/minecraft/util/math/BlockPos;Z)Z"
		)
	)
	private void trySpawningDecayEffects(
		BlockState state, ServerWorld world, BlockPos pos, RandomGenerator random, CallbackInfo ci
	) {
		this.trySpawningDecayEffects(state, world, pos);
	}

	@Unique
	private static Collection<BlockPos> getDiagonalPositions(BlockPos pos) {
		final Collection<BlockPos> diagonalPositions = new LinkedList<>();
		for (final Direction direction : Direction.values()) {
			if (direction.getHorizontal() >= 0) {
				diagonalPositions.add(pos.offset(direction).offset(direction.rotateYClockwise()));
			} else {
				final BlockPos vOffsetPos = pos.offset(direction);
				for (final Direction horizontal : leaves_us_in_peace$getHORIZONTAL()) {
					diagonalPositions.add(vOffsetPos.offset(direction).offset(horizontal.rotateYClockwise()));
				}
			}
		}

		return diagonalPositions;
	}

	@Unique
	private void trySpawningDecayEffects(BlockState state, ServerWorld world, BlockPos pos) {
		if (shouldDoDecayingLeavesEffects()) {
			this.spawnBreakParticles(world, null, pos, state);
		}
	}
}
