package net.sssubtlety.leaves_us_in_peace.mixin;

import net.sssubtlety.leaves_us_in_peace.mixin_helper.PackedTicksMixinAccessor;

import net.minecraft.block.LeavesBlock;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.tick.Tick;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.ArrayList;
import java.util.List;

@Mixin(Chunk.PackedTicks.class)
abstract class PackedTicksMixin implements PackedTicksMixinAccessor {
    @Unique
    private List<Tick<LeavesBlock>> leavesDecayTicks;

    @Override
    public List<Tick<LeavesBlock>> leaves_us_in_peace$getLeavesDecayTicks() {
        return this.leavesDecayTicks;
    }

    @Override
    public void leaves_us_in_peace$setLeavesDecayTicks(List<Tick<LeavesBlock>> leavesDecayTicks) {
        this.leavesDecayTicks = leavesDecayTicks;
    }

    @Inject(method = "<init>", at = @At("TAIL"))
    private void initFields(CallbackInfo ci) {
        // initialize value so non-WorldChunks don't throw NPEs
        this.leavesDecayTicks = new ArrayList<>();
    }
}
