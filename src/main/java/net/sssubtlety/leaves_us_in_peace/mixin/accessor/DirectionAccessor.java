package net.sssubtlety.leaves_us_in_peace.mixin.accessor;

import net.minecraft.util.math.Direction;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(Direction.class)
public interface DirectionAccessor {
    @Accessor("HORIZONTAL")
    static Direction[] leaves_us_in_peace$getHORIZONTAL() { throw new UnsupportedOperationException(); }
}
