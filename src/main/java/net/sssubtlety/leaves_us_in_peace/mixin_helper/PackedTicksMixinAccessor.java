package net.sssubtlety.leaves_us_in_peace.mixin_helper;

import net.minecraft.block.LeavesBlock;
import net.minecraft.world.tick.Tick;

import java.util.List;

public interface PackedTicksMixinAccessor {
    List<Tick<LeavesBlock>> leaves_us_in_peace$getLeavesDecayTicks();

    void leaves_us_in_peace$setLeavesDecayTicks(List<Tick<LeavesBlock>> ticks);
}
