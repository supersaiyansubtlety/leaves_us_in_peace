package net.sssubtlety.leaves_us_in_peace.mixin_helper;

import net.minecraft.block.LeavesBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.tick.WorldTickScheduler;

public interface ServerWorldMixinAccessor {
    WorldTickScheduler<LeavesBlock> leaves_us_in_peace$getLeavesDecayTickScheduler();

    void leaves_us_in_peace$scheduleLeavesDecayTick(BlockPos pos, LeavesBlock leavesBlock, int delay);
}
