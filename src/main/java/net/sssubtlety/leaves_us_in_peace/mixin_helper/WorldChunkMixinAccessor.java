package net.sssubtlety.leaves_us_in_peace.mixin_helper;

import net.minecraft.block.LeavesBlock;
import net.minecraft.world.tick.ChunkTickScheduler;

public interface WorldChunkMixinAccessor {
    void leaves_us_in_peace$setLeavesDecayTickScheduler(ChunkTickScheduler<LeavesBlock> leavesDecayTickScheduler);
}
