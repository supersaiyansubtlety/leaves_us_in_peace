package net.sssubtlety.leaves_us_in_peace.mixin_helper;

import net.minecraft.block.BlockState;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.random.RandomGenerator;

public interface LeavesBlockMixinAccessor {
    void leaves_us_in_peace$tryDecaying(ServerWorld world, BlockPos pos, BlockState state, RandomGenerator random);
}
