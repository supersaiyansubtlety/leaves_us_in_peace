package net.sssubtlety.leaves_us_in_peace;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.registry.Registries;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.registry.tag.BlockTags;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

/*
TODO
 - in future release(s):
	- revive pack maker generation commands
	- datagen
		- use registry to get ids for vanilla blocks
		- for modded content do one of:
			- hardcode strings
			- put in separate projects for each mod and use gradle task to copy generated data
			 (don't have to wait for mods to update)
			 (will loom allow multiple minecraft versions?)
	- custom data type
		- trunk field of logs
		- leaves field of leaves group
		- parse trunk and leaves identically to tag "values" field
		- global required true/false field
		- each trunk block maps to each leaves group
		- each leaves in a leaves group mapped to each other leaves in that group
		-> less redundancy than tags
		-> no need to try to cache tag keys
		- keep support for legacy tags (things like datagen/zenscript support them already)
*/
public class LeavesUsInPeace {
	public static final String NAMESPACE = "leaves_us_in_peace";
	public static final Text NAME = Text.translatable("text." + NAMESPACE + ".name");

	public static final TagKey<Block> LOGS_WITHOUT_LEAVES =
		TagKey.of(RegistryKeys.BLOCK, idOf("logs_without_leaves"));
	public static final TagKey<Block> DECAYS_SLOWLY =
		TagKey.of(RegistryKeys.BLOCK, idOf("decays_slowly"));

	private static final String LEAVES_GROUPS_SUB_PATH = "leaves_groups/";
	private static final String TREE_TYPES_SUB_PATH = "tree_types/";

	private static final Map<Block, TagKey<Block>> LEAVES_GROUPS = new HashMap<>();
	private static final Map<Block, TagKey<Block>> TREE_TYPES = new HashMap<>();

	public static void updateLeavesGroups(Block leavesBlock) {
		updateBlockTag(leavesBlock, LEAVES_GROUPS, LEAVES_GROUPS_SUB_PATH);
	}

	public static void updateTreeTypes(BlockState state) {
		if (state.isIn(BlockTags.LOGS)) {
			updateBlockTag(state.getBlock(), TREE_TYPES, TREE_TYPES_SUB_PATH);
		}
	}

	@Nullable
	public static TagKey<Block> getLeavesGroup(Block leavesBlock) {
		return LEAVES_GROUPS.get(leavesBlock);
	}

	@Nullable
	public static TagKey<Block> getTreeType(Block block) {
		return TREE_TYPES.get(block);
	}

	public static void clearTags() {
		LEAVES_GROUPS.clear();
		TREE_TYPES.clear();
	}

	private static void updateBlockTag(Block block, Map<Block, TagKey<Block>> tagMap, String subPath) {
		if (tagMap.get(block) == null) {
			final Identifier id = Registries.BLOCK.getId(block);
			tagMap.put(block, TagKey.of(RegistryKeys.BLOCK, id.withPrefix(subPath)));
		}
	}

	public static Identifier idOf(String path) {
		return Identifier.of(NAMESPACE, path);
	}

	public interface PackIds {
		Identifier WOOD_PREVENTS_DECAY = idOf("wood_prevents_decay");
		Identifier YUNGS_BETTER_MINESHAFTS_COMPAT = idOf("yungs_better_mineshafts_compat");
	}
}
