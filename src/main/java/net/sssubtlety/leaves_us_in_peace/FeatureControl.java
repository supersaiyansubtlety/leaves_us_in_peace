package net.sssubtlety.leaves_us_in_peace;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.random.RandomGenerator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static net.sssubtlety.leaves_us_in_peace.LeavesUsInPeace.DECAYS_SLOWLY;
import static net.sssubtlety.leaves_us_in_peace.Util.isModLoaded;

public final class FeatureControl {
    private static final Config CONFIG_INSTANCE;

    static {
        CONFIG_INSTANCE = isModLoaded("cloth-config", ">=7.0.72") ?
                AutoConfig.register(Config.class, GsonConfigSerializer::new).getConfig() : null;
    }

    public static final class Defaults {
        private Defaults() { }

        public static final boolean matchLeavesTypes = true;
        public static final boolean unknownLeavesOnlyMatchSelf = true;
        public static final boolean matchLogsToLeaves = true;
        public static final boolean ignorePersistentLeaves = true;
        public static final boolean accelerateLeavesDecay = true;
        public static final int minDecayDelay = 10;
        public static final int maxDecayDelay = 60;
        public static final boolean updateDiagonalLeaves = true;
        public static final boolean doDecayingLeavesEffects = false;
    }

    public static boolean isConfigLoaded() {
        return CONFIG_INSTANCE != null;
    }

    public static boolean shouldMatchLeavesTypes() {
        return CONFIG_INSTANCE == null ? Defaults.matchLeavesTypes : CONFIG_INSTANCE.matchLeavesTypes;
    }

    public static boolean shouldUnknownLeavesOnlyMatchSelf() {
        return CONFIG_INSTANCE == null ? Defaults.unknownLeavesOnlyMatchSelf : CONFIG_INSTANCE.unknownLeavesOnlyMatchSelf;
    }

    public static boolean shouldMatchLogsToLeaves() {
        return CONFIG_INSTANCE == null ? Defaults.matchLogsToLeaves : CONFIG_INSTANCE.matchLogsToLeaves;
    }

    public static boolean shouldIgnorePersistentLeaves() {
        return CONFIG_INSTANCE == null ? Defaults.ignorePersistentLeaves : CONFIG_INSTANCE.ignorePersistentLeaves;
    }

    public static boolean shouldAccelerateLeavesDecay(BlockState state) {
        return !state.isIn(DECAYS_SLOWLY) && (
            CONFIG_INSTANCE == null ?
                Defaults.accelerateLeavesDecay
                : CONFIG_INSTANCE.accelerateLeavesDecay
        );
    }

    public static int getDecayDelay(RandomGenerator random) {
        final int maxDelay = getMaxDecayDelay();

        if (maxDelay <= 0) {
            return 0;
        }

        final int minDelay = getMinDecayDelay();

        return minDelay < maxDelay ?
            random.range(minDelay, maxDelay + 1) : maxDelay;
    }

    public static boolean shouldUpdateDiagonalLeaves() {
        return CONFIG_INSTANCE == null ? Defaults.updateDiagonalLeaves : CONFIG_INSTANCE.updateDiagonalLeaves;
    }

    public static boolean shouldDoDecayingLeavesEffects() {
        return CONFIG_INSTANCE == null ? Defaults.doDecayingLeavesEffects : CONFIG_INSTANCE.doDecayingLeavesEffects;
    }

    public static boolean leavesMatch(@NotNull BlockState leaves1, @NotNull BlockState leaves2) {
        if (leaves1.getBlock() == leaves2.getBlock()) {
            return true;
        } else {
            @Nullable
            final TagKey<Block> leavesGroup = LeavesUsInPeace.getLeavesGroup(leaves1.getBlock());
            if (leavesGroup == null) {
                return !shouldUnknownLeavesOnlyMatchSelf();
            } else {
                return leaves2.isIn(leavesGroup);
            }
        }
    }

    private static int getMinDecayDelay() {
        return CONFIG_INSTANCE == null ? Defaults.minDecayDelay : CONFIG_INSTANCE.decayDelay.minimum;
    }

    private static int getMaxDecayDelay() {
        return CONFIG_INSTANCE == null ? Defaults.maxDecayDelay : CONFIG_INSTANCE.decayDelay.maximum;
    }

    public static void init() { }

    private FeatureControl() { }
}
