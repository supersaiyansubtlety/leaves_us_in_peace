- 1.7.2 (4 Dec. 2024):
  - Marked as compatible with 1.21.4
  - Added tree type tag for pale oaks
  - Minor internal changes
- 1.7.1 (1 Nov. 2024): Fixes errors that could cause world loading to stall
- 1.7.0 (31 Oct. 2024): Updated for 1.21.2-1.21.3
- 1.6.1 (9 Oct. 2024): Port 1.5.1 to 1.21-1.21.1
- 1.5.1 (9 Oct. 2024): Port 1.4.1 to 1.20.2-1.20.6
- 1.4.1 (9 Oct. 2024):
  - Replace [Oh The Biomes You'll Go](<https://modrinth.com/mod/biomesyougo>) compat with
    [Oh The Biomes We've Gone](<https://modrinth.com/mod/oh-the-biomes-weve-gone>) compat
- 1.3.1 (9 Oct. 2024): Port 1.3.0 to 1.19.3-1.19.4
- 1.2.1 (8 Oct. 2024): Updated [Oh The Biomes You'll Go](<https://modrinth.com/mod/biomesyougo>) compat
- 1.6.0 (15 Sep. 2024): Port 1.5.0 to 1.21-1.21.1
- 1.5.0 (15 Sep. 2024): Port 1.4.0 to 1.20.2-1.20.6
- 1.4.0 (15 Sep. 2024):
  - Port 1.3.0 to 1.20-1.20.1
  - Includes fix for Swamp Oak Trees' outermost leaves decaying when they shouldn't
- 1.3.0 (15 Sep. 2024): Port 1.2.0 to 1.19.3-1.19.4
- 1.2.0 (15 Sep. 2024):
  - Substantial internal changes, including a dedicated leaves decay tick scheduler
  - Fixed some [Twilight Forest](<https://www.curseforge.com/minecraft/mc-mods/the-twilight-forest>)
  trees' leaves decaying when they shouldn't
  - Added tags for [Twilight Forest](<https://www.curseforge.com/minecraft/mc-mods/the-twilight-forest>) trees
  - Fixed [Yung's better Mineshafts](<https://modrinth.com/mod/yungs-better-mineshafts>)'
  Overgrown Mineshafts' leaves decaying when they shouldn't
  - Removed builtin "oak_leaves_recognize_jungle_logs" and made it part of mod data instead; if you want to make it so
  Oak Leaves don't recognize Jungle Logs, you can still do so with a datapack that blocks
  `leaves_us_in_peace:jungle_bush_leaves`
  - Added `leaves_us_in_peace:decays_slowly` tag to prevent accelerated decay for certain leaves.
  This can be used to work around modded leaves that don't work with Leaves Us In Peace's accelerated decay.
  Please report any such leaves on the
  [Leaves Us In Peace issue tracker](<https://gitlab.com/supersaiyansubtlety/leaves_us_in_peace/-/issues>).
  - Added [Traverse](<https://modrinth.com/mod/traverse>) blocks to "wood_prevents_decay"
  - Added (translatable) descriptions for builtin data packs
  - Removed the "Download translation updates" config and the bundled CrowdinTranslate mod
- 1.1.3 (28 Aug. 2024): Marked as compatible with 1.21.1
- 1.1.2 (9 Jul. 2024): Added optional [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency for automatic translation updates
- 1.1.1 (28 Jun. 2024): Fixed built-in data packs: fixes jungle oak bushes decaying
- 1.1.0 (13 Jun. 2024):
  - Updated for 1.21
  - Removed bundled crowdin translate mod for now as it hasn't been updated
- 1.0.22 (8 May 2024):
  - Marked as compatible with 1.20.6
  - Improved [Mod Menu](https://modrinth.com/mod/modmenu) integration
- 1.0.21 (23 Apr. 2024): Added tag for cherry tree type
- 1.0.20 (23 Apr. 2024): Marked as compatible with 1.20.5
- 1.0.19 (27 Jan. 2024):
  
  - Marked as compatible with 1.20.3 and 1.20.4
  - Minor internal changes

- 1.0.18 (2 Dec. 2023): Updated for 1.20.2
- 1.0.17 (22 Jun. 2023): Added Traverse compat so its autumnal trees don't decay. Fixes [#17](https://gitlab.com/supersaiyansubtlety/leaves_us_in_peace/-/issues/17)
- 1.0.16 (16 Jun. 2023): Fixed a crash (hopefully)
- 1.0.15 (14 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.0.14 (21 Mar. 2023): Marked as *incompatible* with 1.19-1.19.2
- 1.0.13 (19 Mar. 2023): Updated for 1.19.4
- 1.0.12 (26 Nov. 2022):

  - Added Oh the Biomes You'll Go support.
  - Added wood_prevents_decay datapack (default disabled) which allows wood, in addition to logs, to prevent leaf decay.
  - Fixed promenade woods and stripped wood/logs preventing decay even with wood_prevents_decay disabled.

- 1.0.11 (19 Aug. 2022): Back-ported fix for handling of unknown (modded) tree types to 1.18.2 version
- 1.0.10 (8 Aug. 2022):
  - Added tag for mangrove tree type
  - Added tags for [Promenade](https://modrinth.com/mod/promenade) tree types
- 1.0.9 (6 Aug. 2022): Marked as compatible with 1.19.2
- 1.0.8 (4 Aug. 2022): Fixed handling of unknown (modded) tree types. Modded trees shouldn't explode anymore!
- 1.0.7 (28 Jul. 2022): Marked as compatible with 1.19.1
- 1.0.6 (23 Jun. 2022): Updated for 1.19!
- 1.0.5 (21 May 2022):

  Added builtin datapack "oak_leaves_recognize_jungle_logs" which prevents oak leaves 
  from decaying when they connect to jungle logs. This is to prevent the jungle-logged,
  oak-leaved tree-bushes that generate in jungles from decaying.

  Thanks to [David M](https://gitlab.com/PieKing1215) for reporting this issue!

- 1.0.4 (12 Mar. 2022): Updated for 1.18.2
- 1.0.3 (30 Jan. 2022): Fix crash that could occur when loading leaves that had been loaded in previous version of the mod.
- 1.0.2 (27 Jan. 2022):

  - Fixed persistent leaves' distance never being calculated when 'ignorePersistentLeaves' was enabled.
  - This prevented observers from triggering when observing persistent leaves whose distance should have changed, 
    which is used in some redstone contraptions.
  - Persistent leaves now calculate distance based on both non-persistent and persistent leaves, 
    while non-persistent leaves still ignore persistent leaves (if the config is enabled).

- 1.0.1 (15 Jan. 2022): 
  - Fixed translations downloading on servers.
  - Warnings will no longer be logged when Mod Menu and/or Cloth Config aren't present.
- 1.0 (6 Jan. 2022): First release!
- 0.1 (6 Jan. 2022): Rebranded to Leaves Us In Peace. Add features.